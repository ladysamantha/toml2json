// toml2json
// Copyright (C) 2018  Samantha Enders
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

package toml2json

import (
	"encoding/json"
	"github.com/BurntSushi/toml"
)

// ConvertFile convert file from toml 0.4 to json
func ConvertFile(filePath string) (string, error) {
	var err error
	var result interface{}

	if _, err = toml.DecodeFile(filePath, &result); err != nil {
		return "", err
	}

	buffer, err := json.Marshal(&result)

	if err != nil {
		return "", err
	}

	return string(buffer), nil
}
