// toml2json
// Copyright (C) 2018  Samantha Enders
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/ladysamantha/toml2json"
)

func main() {
	app := cli.NewApp()
	app.Name = "toml2json"
	app.Version = "0.0.1"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "file, f",
			Usage: "file to convert to json",
		},
	}

	app.Action = func(ctx *cli.Context) error {
		fileName := ctx.String("file")
		if fileName == "" {
			return cli.NewExitError("file not provided", 1)
		}

		jsonResult, err := toml2json.ConvertFile(fileName)

		if err != nil {
			return err
		}

		fmt.Println(jsonResult)
		return nil
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
