# toml2json
# Copyright (C) 2018  Samantha Enders
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

SHELL := /bin/bash

default: build lint test

.PHONY: build lint test

build:
	@echo building
	@./scripts/build.sh

lint:
	@echo linting
	@./scripts/lint.sh

test: build
	@echo testing
	@./scripts/test.sh

